# -*- coding: utf-8 -*-

import csv
from datetime import datetime, timedelta
from flask import render_template, Response, stream_with_context
from flask import request, flash, redirect, url_for
from flask.ext.admin import expose
from flask.ext.admin.contrib.sqla import ModelView, filters
from flask.ext.admin.model import typefmt
from flask.ext.admin.actions import action
from flask.ext.babelex import Babel, gettext, lazy_gettext
from flask.ext.security import current_user, login_required
from flask.ext.security.utils import encrypt_password
from wtforms import validators, fields
from wtforms.validators import ValidationError
from flask.ext.wtf import Form
from flask.ext.wtf.file import FileField, file_required, file_allowed
import sqlalchemy
from models import db, WhiteList, Logbook, Ban, add_log
from models import User, Role, WhiteList, Logbook, Ban
from app import app


def is_number(form, field):
    if not (field.data and field.data.isdigit()):
        raise validators.ValidationError(lazy_gettext('Must be a number!'))


def datetime_format(view, value):
    return value.strftime('%d.%m.%Y %H:%M:%S')


MY_DEFAULT_FORMATTERS = dict(typefmt.BASE_FORMATTERS)
MY_DEFAULT_FORMATTERS.update(
    {datetime: datetime_format})

# Fix for column_filters and babel
class FiltersFixMixin(object):

    def scaffold_filters(self, name):
        filters = super(FiltersFixMixin, self).scaffold_filters(name)

        def fix_filter(filter):
            filter.name = filter.name.encode('utf-8')
            return filter

        return map(fix_filter, filters)


# FORMS
class ContactImportForm(Form):
    filename = FileField(gettext('File'), validators=[
        file_required(),
        file_allowed(['csv', 'CSV'])])

    def validate_filename(form, field):
        data = csv.reader(field.data.readlines())
        linenum = 1
        for line in data:
            if len(line) != 5:
                msg = gettext('CSV file is broken, line %(linenum)s',
                        linenum=linenum)
                raise ValidationError(msg)
            elif not line[0].isdigit():
                raise ValidationError(gettext(
                    'The first column does not contain phone '
                    'number, line %(linenum)s', linenum=linenum))
            linenum += 1
        field.data.seek(0)


# Admin
class WhiteListAdmin(FiltersFixMixin, ModelView):
    create_template = 'admin/contact_create.html'
    column_searchable_list = ('number', 'name', 'flat', 'car')
    column_default_sort = 'number'
    column_labels = {
        'name': lazy_gettext('Name'),
        'number': lazy_gettext('Number'),
        'flat': lazy_gettext('Flat'),
        'car': lazy_gettext('Car'),
        'note': lazy_gettext('Note')
    }
    page_size = app.config.get('PAGE_SIZE', 50)

    form_args = {
        'number': dict(validators=[validators.Required(), is_number]),
    }
    def is_accessible(self):
        return current_user.has_role('manager')

    def on_model_change(self, form, model, is_created=False):
        if is_created:
            add_log(gettext('Number %(n)s is added by %(u)s.',
                                 n=model, u=current_user),
                    number=model.number)
        else:
            add_log(gettext('Number %(n)s is updated by %(u)s.',
                                 n=model, u=current_user),
                    number=model.number)

    def on_model_delete(self, model):
        add_log(gettext('Number %(n)s has been deleted by %(u)s',            
                             n=model, u=current_user),
                number=model.number)


    @expose('/import', methods=['POST', 'GET'])
    def import_contacts(self):
        form = ContactImportForm()
        if request.method == 'GET':
            return self.render('admin/contact_import.html', form=form)

        else:
            form = ContactImportForm()
            if form.validate_on_submit():
                data = csv.reader(form.filename.data.readlines())
                imported_count = 0
                imported_list = []
                existing_count = 0
                existing_list = []
                for line in data:
                    # Skip existing contacts
                    if WhiteList.query.filter_by(number=line[0]).first():
                        existing_list.append(line[0])
                        existing_count += 1
                        continue

                    c = WhiteList()
                    c.number = line[0]
                    c.name = line[1].decode('utf-8')
                    c.flat = line[2].decode('utf-8')
                    c.car = line[3].decode('utf-8')
                    c.note = line[4].decode('utf-8')
                    db.session.add(c)
                    imported_count += 1
                    imported_list.append(c.number)
                try:
                    db.session.commit()
                except sqlalchemy.exc.IntegrityError, e:
                    params = u','.join(e.params)
                    flash(u'%s: %s' % (e.message, params), 'error')
                    db.session.rollback()
                    return redirect(url_for('.index_view'))

                if imported_count:
                    flash(gettext('Imported %(num)s numbers: %(list)s',
                              num=imported_count,
                              list='\n'.join(imported_list)))
                if existing_count:
                    # We have some existing contacts
                    flash(gettext('Skipped %(num)s of existing numbers: %(list)s',
                                  num=existing_count,
                                  list='\n'.join(existing_list)))
                return redirect(url_for('.index_view'))

            else:
                return self.render('admin/contact_import.html', form=form)





class WhiteListView(FiltersFixMixin, ModelView):
    column_searchable_list = ('number', 'name', 'flat', 'car')
    can_edit = can_create = can_delete = False
    column_labels = {
        'name': lazy_gettext('Name'),
        'number': lazy_gettext('Number'),
        'flat': lazy_gettext('Flat'),
        'car': lazy_gettext('Car'),
        'note': lazy_gettext('Note')
    }

    form_args = {
        'number': dict(validators=[validators.Required()]),
    }
    def is_accessible(self):
        return current_user.has_role('analytics') and \
            not current_user.has_role('manager')


class LogbookView(FiltersFixMixin, ModelView):
    column_searchable_list = ('message',)
    can_delete = False
    can_edit = False
    can_create = False
    column_default_sort = 'created'
    column_type_formatters = MY_DEFAULT_FORMATTERS
    column_list = ['created', 'number', 'message']
    column_labels = dict(
        created=lazy_gettext('Created'),
        number=lazy_gettext('Number'),
        message=lazy_gettext('Message'),

    )
    column_filters = ['number', 'created', 'message']

    def is_accessible(self):
        return current_user.has_role('analytics') and \
            not current_user.has_role('manager')


class LogbookAdmin(LogbookView):
    can_delete = True

    def is_accessible(self):
        return current_user.has_role('manager')

    @action('delete_all', lazy_gettext('Delete All'),
            lazy_gettext('Are you sure you want to delete all records?'))
    def action_delete_all(self, ids):
        for log in Logbook.query.all():
            db.session.delete(log)
        db.session.commit()

    @action('export_all', lazy_gettext('Export All'))
    def action_export_all(self, ids):
        def generate():
            for log in Logbook.query.all():
                yield '%s,%s,%s\n' % (
                    log.created.strftime('%d.%m.%y %H:%M:%S'),
                    log.number, log.message)
        resp = Response(stream_with_context(generate()), mimetype='text/csv')
        file_date = datetime.now().strftime('%d.%m.%y')
        resp.headers['Content-Disposition'] = "attachment; " + \
                                "filename='logbook-%s.csv'" % file_date
        return resp

class BanView(FiltersFixMixin, ModelView):
    can_delete = False
    can_create = False
    can_edit = False
    column_type_formatters = MY_DEFAULT_FORMATTERS
    column_labels = {
        'added': lazy_gettext('Added'),
        'number': lazy_gettext('Number'),
    }

    def is_accessible(self):
        return current_user.has_role('analytics') and \
            not current_user.has_role('manager')


class BanAdmin(BanView):
    can_delete = True

    def is_accessible(self):
        return current_user.has_role('manager')


class UserAdmin(ModelView):
    column_auto_select_related = True
    can_view_details = True
    form_widget_args = {
        'current_login_at': dict(disabled=True),
        'current_login_ip': dict(disabled=True),
        'login_count': dict(disabled=True),
    }
    form_excluded_columns = ['password']
    column_exclude_list = ['password']
    column_list = ['email', 'roles', 'active', 'current_login_at',
                   'current_login_ip', 'login_count']
    column_labels = {
        'email': lazy_gettext('Email'),
        'password': lazy_gettext('Password'),
        'active': lazy_gettext('Active'),
        'confirmed_at': lazy_gettext('Confirmed At'),
        'roles': lazy_gettext('Roles'),
        'current_login_at': lazy_gettext('Current Login At'),
        'current_login_ip': lazy_gettext('Current Login IP'),
        'login_count': lazy_gettext('Login Count'),
    }

    def scaffold_form(self):
        form_class = super(UserAdmin, self).scaffold_form()
        form_class.password2 = fields.PasswordField(lazy_gettext('New Password'))
        return form_class

    def on_model_change(self, form, model, is_created=False):
        if len(model.password2):
            model.password = encrypt_password(model.password2)
            
        if is_created:
            add_log(gettext('User %(u)s with roles %(r)s has been created by %(c)s.',
                                 u=model,
                                 r=','.join([k.name for k in model.roles]),
                                 c=current_user))
        else:
            add_log(gettext('User %(u)s has been updated by %(c)s.',
                                 u=model, c=current_user))

    def on_model_delete(self, model):
        add_log(gettext('User %(u)s has been deleted by %(c)s.',
                             u=model, c=current_user))

    def is_accessible(self):
        return current_user.has_role('admin')


def add_admin_views(app):
    admin = app.admin
    admin.add_view(WhiteListAdmin(WhiteList, db.session,
                   name=lazy_gettext('White List'),
                   menu_icon_type='glyph',
                   menu_icon_value='glyphicon-list-alt'))
    admin.add_view(WhiteListView(WhiteList, db.session,
                   name=lazy_gettext('White List'),
                   endpoint='whitelistview',
                   menu_icon_type='glyph',
                   menu_icon_value='glyphicon-list-alt'))

    admin.add_view(BanAdmin(Ban, db.session,
                   name=lazy_gettext('Bans'),
                   menu_icon_type='glyph',
                   menu_icon_value='glyphicon-minus-sign'))
    admin.add_view(BanView(Ban, db.session,
                   name=lazy_gettext('Bans'),
                   endpoint='banview',
                   menu_icon_type='glyph',
                   menu_icon_value='glyphicon-minus-sign'))

    admin.add_view(LogbookAdmin(Logbook, db.session,
                                name=lazy_gettext('Logbook'),
                                menu_icon_type='glyph',
                                menu_icon_value='glyphicon-book'))
    admin.add_view(LogbookView(Logbook, db.session,
                               name=lazy_gettext('Logbook'),
                               endpoint='logbookview',
                               menu_icon_type='glyph',
                               menu_icon_value='glyphicon-book'))
    admin.add_view(UserAdmin(User, db.session,
                             name=lazy_gettext('Users'),
                             menu_icon_type='glyph',
                             menu_icon_value='glyphicon-user'))
