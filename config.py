DEBUG = False ### NEVER EVER SET True IN PRODUCTION!!!
SECRET_KEY = 'change_me_to_random_key'
DEFAULT_LANGUAGE = 'ru'

REPORT_SEND_TO = ['youremail@here.com', 'second@email.here.com']

PAGE_SIZE = 50 # Pagination number of records

BAN_RULES = {
    'calls': 2,
    'interval': 60,
    'period': 300
}
# How many calls per interval will trigger ban for period specified.


MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 587
MAIL_USE_SSL = False
MAIL_USE_TLS = True
MAIL_USERNAME = 'auth@gmail.com'
MAIL_PASSWORD = 'password'


SQLALCHEMY_DATABASE_URI = 'sqlite:///./database.db'

SECURITY_TRACKABLE = True
SECURITY_CONFIRMABLE = False
SECURITY_REGISTERABLE = False
SECURITY_RECOVERABLE = False
SECURITY_PASSWORD_HASH = 'pbkdf2_sha512'
SECURITY_PASSWORD_SALT = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
SECURITY_POST_LOGIN_VIEW = "/"
SECURITY_POST_LOGOUT_VIEW = "/"
