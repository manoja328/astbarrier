from datetime import datetime
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.security import UserMixin, RoleMixin

db = SQLAlchemy()

roles_users = db.Table('roles_users',
        db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
        db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))

class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __str__(self):
        return '%s' % self.name

    def __unicode__(self):
        return self.__str__()


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))
    current_login_at = db.Column(db.DateTime())
    current_login_ip = db.Column(db.String(15))
    login_count = db.Column(db.Integer, default=0)

    def __str__(self):
        return '%s' % self.email



class WhiteList(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    number = db.Column(db.String(30), unique=True)
    name = db.Column(db.String(100), index=True)
    flat = db.Column(db.String(100))
    car = db.Column(db.String(255))
    note = db.Column(db.Text)

    def __str__(self):
        return '%s <%s>' % (self.name, self.number)


class Ban(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    added = db.Column(db.DateTime, default=datetime.now, index=True)
    number = db.Column(db.String(30), unique=True)


class Logbook(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.DateTime, index=True, default=datetime.now)
    message = db.Column(db.Text, index=True)
    number = db.Column(db.String(30), index=True)
    is_accepted = db.Column(db.Boolean)


def add_log(msg, number=None, is_accepted=False):
    log = Logbook(number=number, message=msg, is_accepted=is_accepted)
    db.session.add(log)
    db.session.commit()
