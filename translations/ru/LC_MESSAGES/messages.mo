��    8      �  O   �      �     �     �  ,   �          "     '     /     F  $   V     {          �     �     �  
   �     �  )   �  6   �  
   (     3     8     =  4   M  "   �     �     �     �     �     �     �     �     �     �     �       &        /  !   O  $   q  .   �     �  #   �     �  -   �  @   &  %   g  %   �  6   �     �  
   �     �     	     &	  	   ,	     6	  �  >	          )  K   <     �     �     �     �  "   �  3        5     >     W  &   f  )   �     �  !   �  ,   �  e     #   �     �     �  +   �  {   �  ;   h     �  
   �     �  
   �  !   �     �  !        '     .     F  
   [  @   f  C   �  O   �  7   ;  7   s     �  ?   �     �  U     i   W  M   �  ]     c   m     �     �       .     
   N     Y     j                     8           )   .                                    	   '   7                         1       6      3      $      -   0   /             &   "                 #             5                ,             2              !   (      %   4   *   +                    
    Active Added Are you sure you want to delete all records? Authentication Bans Barrier Barrier: %(barrier)s.  Barriers Access CSV file is broken, line %(linenum)s Car Confirmed At Created Current Login At Current Login IP Delete All Email Entry request from %(number)s - accepted. Entry request from unknown number %(number)s: refused. Export All File Flat Import Contacts Import Contacts from a CSV file (phone, name, note): Imported %(num)s numbers: %(list)s Language Log Out Logbook Login Login Count Message Must be a number! Name New Password Note Number Number %(n)s has been deleted by %(u)s Number %(n)s is added by %(u)s. Number %(n)s is updated by %(u)s. Number banned for %(period)s seconds Number has been unbanned, request is accepted. Password Request refused - number is banned. Roles Skipped %(num)s of existing numbers: %(list)s The first column does not contain phone number, line %(linenum)s User %(u)s has been deleted by %(c)s. User %(u)s has been updated by %(c)s. User %(u)s with roles %(r)s has been created by %(c)s. Users White List You can also [Barrier] Daily report for %s admin analytics manager Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2015-09-29 14:17+0300
PO-Revision-Date: 2015-09-29 14:48+0300
Last-Translator: 
Language: ru
Language-Team: ru <LL@li.org>
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.1.1
X-Generator: Poedit 1.8.5
 Активный Добавлено Вы уверены что хотите удалить все записи? Аутентификация Баны Шлагбаум Шлагбаум: %(barrier)s. Доступ к шлагбауму CSV файл кривой, строка %(linenum)s Авто Подтверждено Создано Время текущего входа IP адрес текущего входа Удалить Все Электронный адрес Запрос от %(number)s - принят. Запрос на въезд от неизвестного номера %(number)s: отказано. Экспортировать Все Файл Квартира Импортировать контакты Импортировать контакты из CSV файла (номер телефона, имя, примечание) Импортировано %(num)s номеров: %(list)s Язык Выйти Журнал Войти Количество входов Сообщение Должен быть номер! Имя Новый пароль Примечание Номер Номер %(n)s удален пользователем %(u)s . Номер %(n)s добавлен пользователем %(u)s. Номер %(n)s отредактирован пользователем %(u)s. Номер забанен на %(period)s секунд. Номер разбанен, запрос принят. Пароль В запросе отказано - номер забанен. Роли Пропущено %(num)s ранее добавленных номеров: %(list)s Первая колонка не содержит номера телефона, строка %(linenum)s Пользователь %(u)s удален пользователем %(c)s. Пользователь %(u)s отредактирован пользователем %(c)s. Пользователь %(u)s с ролями %(r)s создан пользователем %(c)s. Пользователи Белый Список Вы можете также [Шлагбаум] Отчет за день %s админ аналитик менеджер 