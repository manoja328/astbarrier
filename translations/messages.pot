# Translations template for PROJECT.
# Copyright (C) 2015 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2015-09-29 14:17+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.1.1\n"

#: app.py:44
#, python-format
msgid "Barrier: %(barrier)s. "
msgstr ""

#: app.py:49
#, python-format
msgid "Entry request from unknown number %(number)s: refused."
msgstr ""

#: app.py:58
msgid "Request refused - number is banned."
msgstr ""

#: app.py:65
msgid "Number has been unbanned, request is accepted."
msgstr ""

#: app.py:77
#, python-format
msgid "Number banned for %(period)s seconds"
msgstr ""

#: app.py:82
#, python-format
msgid "Entry request from %(number)s - accepted."
msgstr ""

#: app.py:87
msgid "Barrier"
msgstr ""

#: manage.py:41
#, python-format
msgid "[Barrier] Daily report for %s"
msgstr ""

#: views.py:26
msgid "Must be a number!"
msgstr ""

#: views.py:52
msgid "File"
msgstr ""

#: views.py:61
#, python-format
msgid "CSV file is broken, line %(linenum)s"
msgstr ""

#: views.py:65
#, python-format
msgid "The first column does not contain phone number, line %(linenum)s"
msgstr ""

#: views.py:78 views.py:166
msgid "Name"
msgstr ""

#: views.py:79 views.py:167 views.py:191 views.py:235
msgid "Number"
msgstr ""

#: views.py:80 views.py:168
msgid "Flat"
msgstr ""

#: views.py:81 views.py:169
msgid "Car"
msgstr ""

#: views.py:82 views.py:170
msgid "Note"
msgstr ""

#: views.py:94
#, python-format
msgid "Number %(n)s is added by %(u)s."
msgstr ""

#: views.py:98
#, python-format
msgid "Number %(n)s is updated by %(u)s."
msgstr ""

#: views.py:103
#, python-format
msgid "Number %(n)s has been deleted by %(u)s"
msgstr ""

#: views.py:147
#, python-format
msgid "Imported %(num)s numbers: %(list)s"
msgstr ""

#: views.py:152
#, python-format
msgid "Skipped %(num)s of existing numbers: %(list)s"
msgstr ""

#: views.py:190
msgid "Created"
msgstr ""

#: views.py:192
msgid "Message"
msgstr ""

#: views.py:208
msgid "Delete All"
msgstr ""

#: views.py:209
msgid "Are you sure you want to delete all records?"
msgstr ""

#: views.py:215
msgid "Export All"
msgstr ""

#: views.py:234
msgid "Added"
msgstr ""

#: views.py:263
msgid "Email"
msgstr ""

#: views.py:264
msgid "Password"
msgstr ""

#: views.py:265
msgid "Active"
msgstr ""

#: views.py:266
msgid "Confirmed At"
msgstr ""

#: views.py:267
msgid "Roles"
msgstr ""

#: views.py:268
msgid "Current Login At"
msgstr ""

#: views.py:269
msgid "Current Login IP"
msgstr ""

#: views.py:270
msgid "Login Count"
msgstr ""

#: views.py:275
msgid "New Password"
msgstr ""

#: views.py:285
#, python-format
msgid "User %(u)s with roles %(r)s has been created by %(c)s."
msgstr ""

#: views.py:288
#, python-format
msgid "User %(u)s has been updated by %(c)s."
msgstr ""

#: views.py:292
#, python-format
msgid "User %(u)s has been deleted by %(c)s."
msgstr ""

#: views.py:302 views.py:306
msgid "White List"
msgstr ""

#: views.py:312 views.py:316
msgid "Bans"
msgstr ""

#: views.py:322 views.py:326
msgid "Logbook"
msgstr ""

#: views.py:331
msgid "Users"
msgstr ""

#: templates/my_master.html:21
msgid "Log Out"
msgstr ""

#: templates/my_master.html:30
msgid "Language"
msgstr ""

#: templates/admin/contact_create.html:5
msgid "You can also"
msgstr ""

#: templates/admin/contact_create.html:5
msgid "Import Contacts"
msgstr ""

#: templates/admin/contact_import.html:5
msgid "Import Contacts from a CSV file (phone, name, note):"
msgstr ""

#: templates/admin/index.html:24
msgid "Barriers Access"
msgstr ""

#: templates/admin/index.html:26
msgid "Authentication"
msgstr ""

#: templates/admin/index.html:29
msgid "Login"
msgstr ""

